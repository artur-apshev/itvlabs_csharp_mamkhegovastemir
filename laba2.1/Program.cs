﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba2._1
{
    class Program
    {    /* Составьте программу вычисления стоимости поездки на автомобиле на дачу (туда и обратно). Исходными данными являются: расстояние до дачи (в километрах); количество бензина, которое потребляет автомобиль на 100 км пробега; цена одного литра бензина. Ниже представлен рекомендуемый вид диалога во время работы программы:
            Вычисление стоимости поездки на дачу.
            Расстояние до дачи (км) – 67
            Расход бензина (л на 100 км) – 8.5
            Цена литра бензина (руб.) – 23.7
            Поездка на дачу обойдется в 269 руб. 94 коп.
            */
        static void Main(string[] args)
        {
            double rast, benzin;
            decimal price, result;

            Console.WriteLine("Вычисление стоимости поездки на дачу.");
            Console.Write("Расстояние до дачи (км) – ");
            rast = double.Parse(Console.ReadLine());
            Console.Write("Расход бензина (л на 100 км) – ");
            benzin = double.Parse(Console.ReadLine());
            Console.Write("Цена литра бензина (руб.) – ");
            price = decimal.Parse(Console.ReadLine());

            result = (decimal)((benzin / 100) * rast) * price * 2;
            int rub = (int)result;
            int cop = (int)((result - rub) * 100);

            Console.WriteLine($"Поездка на дачу обойдется в {rub} руб. {cop} коп.");
            Console.ReadLine();
        }
    }
}
