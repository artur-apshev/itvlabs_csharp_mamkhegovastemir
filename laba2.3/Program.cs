﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba2._3
{
    class Program
    { /*•Составьте программу, которая преобразует введенное с клавиатуры дробное число в денежный формат.
        •Например, число 12,348 должно быть преобразовано к виду 12 руб. 35 коп.
        •Ниже представлен рекомендуемый вид диалога во время работы программы. 
        •Преобразование числа в денежный формат.
        •Введите дробное число – 23,6
        •23.6 руб. – это 23 руб. 60 коп. 
       */
        static void Main(string[] args)
        {
            decimal convert = decimal.Parse(Console.ReadLine());
            convert = Math.Round(convert, 2);
            int rub = (int)convert;
            int cop = (int)((convert - rub) * 100);
            if (cop == 99)
            {
                rub++;
                cop = 0;
            }
            Console.WriteLine($"{rub} rub. {cop} cop");
            Console.ReadLine();
        }
    }
}
