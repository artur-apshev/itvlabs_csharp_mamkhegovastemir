﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using laba3;

namespace TestLaba3
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestFillUp()
        {
            int[] a = new int[100];
            Program.FillUp(a);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] < a[i + 1]);

            }

        }
        [TestMethod]
        public void TestFillDown()
        {
            int[] a = new int[100];
            Program.FillDown(a);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] > a[i + 1]);

            }

        }
        [TestMethod]
        public void TestBubleSort()
        {
            int[] a = new int[1000];
            Program.FillRandom(a);
            Program.BubbleSort(a);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
        [TestMethod]
        public void TestMergeSort()
        {
            int[] a = new int[1000];
            long countOperation=0;
            Program.FillRandom(a);
            Program.MergeSort(a,0,a.Length-1,ref countOperation );
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
        [TestMethod]
        public void TestBubleSort2()
        {
            int[] a = new int[10000];
            Program.FillRandom(a);
            Program.BubbleSort(a);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
        [TestMethod]
        public void TestMergeSort2()
        {
            int[] a = new int[10000];
            long countOperation = 0;
            Program.FillRandom(a);
            Program.MergeSort(a, 0, a.Length - 1, ref countOperation);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
        [TestMethod]
        public void TestBubleSort3()
        {
            int[] a = new int[100000];
            Program.FillRandom(a);
            Program.BubbleSort(a);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
        [TestMethod]
        public void TestMergeSort3()
        {
            int[] a = new int[100000];
            long countOperation = 0;
            Program.FillRandom(a);
            Program.MergeSort(a, 0, a.Length - 1, ref countOperation);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
        [TestMethod]
        public void TestBubleSort4()
        {
            int[] a = new int[1000000];
            Program.FillRandom(a);
            Program.BubbleSort(a);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
        [TestMethod]
        public void TestMergeSort4()
        {
            int[] a = new int[1000000];
            long countOperation = 0;
            Program.FillRandom(a);
            Program.MergeSort(a, 0, a.Length - 1, ref countOperation);
            for (int i = 0; i < a.Length - 1; i++)
            {
                Assert.IsTrue(a[i] <= a[i + 1]);
            }
        }
    }
}



